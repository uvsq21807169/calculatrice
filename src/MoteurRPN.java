import java.util.*;
public class MoteurRPN {
	
	Stack<Double> operandes = new Stack<>(); 
	
	public void enregistrer(Double o) {
		operandes.push(o);
	}
	
	public void calculer(String operateur) {
		
		Operation op;
		
		double result;
		Double a, b;
		switch(operateur) {
			case "+":
				a = operandes.pop();
				b = operandes.pop();
				result = Operation.PLUS.eval(a, b);
				operandes.push(result);
				break;
			case "-":
				a = operandes.pop();
				b = operandes.pop();
				result = Operation.MOINS.eval(a, b);
				operandes.push(result);
				break;
			case "*":
				a = operandes.pop();
				b = operandes.pop();
				result = Operation.MULT.eval(a, b);
				operandes.push(result);
				break;
			case "/":
				a = operandes.pop();
				b = operandes.pop();
				result = Operation.DIV.eval(b, a);
				operandes.push(result);
				break;
			default:
				System.out.println("error");
		}
		for(Double d : operandes)
			System.out.println(d + " ");

		
	}

}
