
public enum Operation {
	PLUS("+"){
		double eval(double a, double b){
			return a + b;
		}
	},
	MOINS("-"){
		double eval(double a, double b){
			return a - b;
		}
	},
	MULT("*"){
		double eval(double a, double b){
			return a * b;
		}
	},
	DIV("/"){
		double eval(double a, double b){
			return a / b;
		}
	};
	
	private String symbole;
	
	private Operation(String s){
		symbole = s;
	}
	
	abstract double eval(double a, double b);
	

}
