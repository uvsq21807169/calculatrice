import java.util.Scanner;

public class SaisieRPN {

	public SaisieRPN(){
		
		Scanner sc = new Scanner(System.in);
		String input = "";
		
		MoteurRPN moteur = new MoteurRPN();
		
		while( ! input.toUpperCase().equals("EXIT")) {
			input = sc.nextLine();
			try{
			    Double.parseDouble(input);
			    moteur.enregistrer(Double.parseDouble(input));
				

			}
			catch(NumberFormatException e){
				moteur.calculer(input);

			}
		}
		
		sc.close();
	}
	
}
